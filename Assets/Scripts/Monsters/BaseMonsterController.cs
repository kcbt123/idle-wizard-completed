using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BaseMonsterController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;

    public string monsterHP;

    [SerializeField]
    private TextMeshProUGUI monsterHPLabel;

    [SerializeField]
    private GameObject monsterHPBarObject;

    private int trueCurrentStage = 1;

    /** ======= MARK: - MonoBehavior Functions ======= */

    void Start()
    {
        AddListeners();
        SetupMonsterHP(StageDataManager.instance.currentStage);
    }

    // Update is called once per frame
    void Update()
    {

    }

    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
            CustomEventSystem.instance.AddListener(EventCode.ON_SHOT_CONTACT, this, OnShotContact),
            CustomEventSystem.instance.AddListener(EventCode.ON_NEXT_STAGE, this, OnNextStage)
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Listeners ======= */
    private void OnNextStage(object[] eventParam)
    {
        if (eventParam.Length > 0)
        {
            trueCurrentStage = int.Parse((string)eventParam[0]);
        } else
        {
            trueCurrentStage = StageDataManager.instance.currentStage;
        }
        SetupMonsterHP(trueCurrentStage);
    }

    // Tat ca incoming shots dung chung 1 ham nhan, check luon loai shot o trong eventParam
    private void OnShotContact(object[] eventParam)
    {
        ShotType type = (ShotType)eventParam[0];
        string shotDamage = (string)eventParam[1];

        switch (type)
        {
            case ShotType.WIZARD:
                Debug.Log("= Received Wizard Shot with damage: " + shotDamage);
                break;
            case ShotType.FAMILLIAR:
                Debug.Log("= Received Familliar Shot with damage: " + shotDamage);
                break;
            case ShotType.ULTIMATE:
                Debug.Log("= Received Ultimate Shot with damage: " + shotDamage);
                break;
        }
        if ((LargeNumberStaticUtil.CompareNumber(monsterHP, shotDamage) == LargeNumberResult.TRUE))
        {
            // HieuBT: - Tinh ratio dua tren damage voi HP goc. sau do giam slider theo
            float ratioReverse = LargeNumberStaticUtil.Division(StageDataManager.instance.CalculateMonsterHP(trueCurrentStage), shotDamage);
            monsterHPBarObject.GetComponent<Slider>().value -= (1 / ratioReverse);

            // HieuBT: - Tru HP hien tai di so voi damage va hien thi cho nguoi choi
            if (LargeNumberStaticUtil.Subtraction(monsterHP, shotDamage) != Constants.LARGE_NUMBER_CANT_CALCULATE)
            {
                monsterHP = LargeNumberStaticUtil.Subtraction(monsterHP, shotDamage);
                monsterHPLabel.text = monsterHP.ToString();
            }
        } else
        {
            monsterHP = "0";
        }

        if (monsterHP == "0")
        {
            monsterHPLabel.text = "0.00";
            monsterHPBarObject.GetComponent<Slider>().value = 0;
            CustomEventSystem.instance.DispatchEvent(EventCode.ON_BOSS_DEFEATED, new object[] {

            });
        }
    }

    /** ======= MARK: - Handle Monster HP ======= */

    private void SetupMonsterHP(int currentStage)
    {
        monsterHP = StageDataManager.instance.CalculateMonsterHP(currentStage);
        monsterHPBarObject.GetComponent<Slider>().value = 1;
        monsterHPLabel.text = monsterHP.ToString();
    }
}
