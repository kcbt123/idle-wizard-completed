using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEditor;
public class CameraController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    public Transform target;
    public Transform bg1;
    public Transform bg2;

    public Text NotiMes; 
    private float size;

    /** ======= MARK: - MonoBehaviour Methods  ======= */
    void Start()
    {
        size = bg1.GetComponent<BoxCollider2D>().size.y;
    }

    // Update is called once per frame
    void Update()
    {
        HandleFixedResolution();

        Vector3 targetPos = new Vector3(target.position.x, target.position.y, transform.position.z);
        transform.position = targetPos;
        if (transform.position.y > bg2.position.y)
        {
            bg1.position = new Vector3(bg2.position.x, bg2.position.y + size, bg1.position.z);
            SwitchBackGround();
        }
    }

    void OnApplicationQuit()
    {
        Debug.Log("Application ending time " + DateTime.Now);
        //GameOffBonus.SetLogOutDT();
    }

    public void SwitchBackGround()
    {
        Transform temp = bg1;
        bg1 = bg2;
        bg2 = temp;
    }

    /** ======= MARK: - Fixed Resolution Function ======= */

    private int lastWidth = 0;
    private int lastHeight = 0;

    private void HandleFixedResolution()
    {
        var width = Screen.width; var height = Screen.height;

        // HieuBT: - Call SetResolution only if current width and height ratio is 9 / 16
        // HieuBT: - Final version can have size larger than this
        // HieuBT: - For the test version, let maximum size be 720x1280

        float forcedRatio = 0.5625f; // Which is 9/16
        float currentRatio = (float)(width / height);

        if (width >= 720.0 || height >= 1280.0 || currentRatio != forcedRatio)
        {
            Screen.SetResolution(lastWidth, lastHeight, false, 0);
        }

        if (lastWidth != width) // HieuBT: - If the user is changing the width
        {
            // HieuBT: - Update the height
            var heightAccordingToWidth = width / forcedRatio;
            Screen.SetResolution(width, (int)Mathf.Round((float)heightAccordingToWidth), false, 0);
        }
        else if (lastHeight != height) // HieuBT: - If the user is changing the height
        {
            // HieuBT: - Update the width
            var widthAccordingToHeight = height / (1 / forcedRatio);
            Screen.SetResolution((int)Mathf.Round((float)widthAccordingToHeight), height, false, 0);
        }
        lastWidth = width;
        lastHeight = height;
    }
}
