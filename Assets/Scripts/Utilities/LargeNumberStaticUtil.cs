using UnityEngine;
using System.Text;
using System;
public class LargeNumberStaticUtil
{
    public static string Addition(string number, string addedNumber)
    {
        if (CompareNumber(number, addedNumber) ==LargeNumberResult.FALSE)
        {
            return Addition(addedNumber, number);
        }
        if (CompareNumber(number, addedNumber) == LargeNumberResult.DONTKNOW)
        {
            return Constants.LARGE_NUMBER_CANT_CALCULATE;
        }
        if (!SameStringTail(number, addedNumber))
        {
            addedNumber = ConvertNumber(addedNumber);
        }
        float fNumber = 0;
        float fAddedNumber = 0;

        ExtractStringNumber(ref number, ref fNumber);
        ExtractStringNumber(ref addedNumber, ref fAddedNumber);

        if (number == addedNumber)
        {
            fNumber += fAddedNumber;
            while (fNumber > 1000)
            {
                fNumber = fNumber / 1000;
                if (number == "") return fNumber.ToString("F2") + "a";
                if (number.Length == 1)
                {
                    if (number[0] == 'z')
                    {
                        number = "aa";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(number);
                        sb[0]++;
                        number = sb.ToString();
                    }
                }
                else
                {
                    if (number[1] == 'z')
                    {
                        StringBuilder sb = new StringBuilder(number);
                        sb[0]++;
                        sb[1] = 'a';
                        number = sb.ToString();
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(number);
                        sb[1]++;
                        number = sb.ToString();
                    }
                }
            }
        }
        if(fNumber<1)return ConvertToSmalllerNumber(fNumber, number);
        return fNumber.ToString("F2") + number;
    }

    public static string Subtraction(string number, string subtractedNumber)
    {
        LargeNumberResult compareResult = CompareNumber(number, subtractedNumber);
        if (compareResult == LargeNumberResult.FALSE)
        {
            return "0";
        }
        if (compareResult == LargeNumberResult.DONTKNOW)
        {
            return Constants.LARGE_NUMBER_CANT_CALCULATE;
        }
        float fNumber = 0;
        float fSubtractedNumber = 0;
        if (!SameStringTail(number, subtractedNumber))
        {
            subtractedNumber = ConvertNumber(subtractedNumber);
        }
        ExtractStringNumber(ref number, ref fNumber);
        ExtractStringNumber(ref subtractedNumber, ref fSubtractedNumber);

        if (number == subtractedNumber)
        {
            if (fNumber == fSubtractedNumber) return "0";
            fNumber -= fSubtractedNumber;
            if (number.Length == 1)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (number[0] == 'a')
                    {
                        number = "";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(number);
                        sb[0]--;
                        number = sb.ToString();
                    }
                }
            }
            if (number.Length == 2)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (number[1] == 'a')
                    {
                        if (number[0] == 'a')
                        {
                            number = "z";
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder(number);
                            sb[0]--;
                            sb[1] = 'z';
                            number = sb.ToString();
                        }
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(number);
                        sb[1]--;
                        number = sb.ToString();
                    }
                }
            }
        }
        if (fNumber == 0 && number == "")
        {
            return "0";
        }
        if (fNumber < 1) return ConvertToSmalllerNumber(fNumber, number);
        return fNumber.ToString("F2") + number;
    }

    public static string Multiplication(string number, float multiplier)
    {
        float fNumber = 0;
        ExtractStringNumber(ref number, ref fNumber);
        fNumber *= multiplier;
        while (fNumber > 1000)
        {
            fNumber = fNumber / 1000;
            if (number == "")
            {
                number = "a";
                continue;
            }
            if (number.Length == 1)
            {
                if (number[0] == 'z')
                {
                    number = "aa";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(number);
                    sb[0]++;
                    number = sb.ToString();
                }
            }
            else
            {
                if (number[1] == 'z')
                {
                    StringBuilder sb = new StringBuilder(number);
                    sb[0]++;
                    sb[1] = 'a';
                    number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(number);
                    sb[1]++;
                    number = sb.ToString();
                }
            }
        }
        if (fNumber < 1) return ConvertToSmalllerNumber(fNumber, number);
        return fNumber.ToString("F2") + number;
    }

    // DungDA: - Check if number >= comparedNumber
    public static LargeNumberResult CompareNumber(string number, string comparedNumber)
    {
        float fNumber = 0;
        float fComparedNumber = 0;
        string sNumber = number;
        string sComparedNumber = comparedNumber;
        bool ConvertToSmallNumber=false;
        if(!SameStringTail(sNumber, sComparedNumber))
        {
            ConvertToSmallNumber = true;
            sNumber = ConvertNumber(sNumber);
            sComparedNumber = ConvertNumber(sComparedNumber);
        }
        ExtractStringNumber(ref sNumber, ref fNumber);
        ExtractStringNumber(ref sComparedNumber, ref fComparedNumber);
        if (sNumber == "" && sComparedNumber == "")
        {
            if(ConvertToSmallNumber && fNumber== fComparedNumber) return LargeNumberResult.DONTKNOW;
            if (fNumber >= fComparedNumber) { return LargeNumberResult.TRUE; } else return LargeNumberResult.FALSE;
        }
        if (sNumber.Length > sComparedNumber.Length)
        {
            return LargeNumberResult.TRUE;
        }
        if (sNumber.Length == sComparedNumber.Length)
        {
            if (sNumber[0] > sComparedNumber[0])
            {
                return LargeNumberResult.TRUE;
            }
            if (sNumber[0] == sComparedNumber[0])
            {
                if (sNumber.Length == 2)
                {
                    if (sNumber[1] > sComparedNumber[1]) return LargeNumberResult.TRUE;
                    if (sNumber[1] < sComparedNumber[1]) return LargeNumberResult.FALSE;
                    if (sNumber[1] == sComparedNumber[1] && ConvertToSmallNumber && fNumber == fComparedNumber) return LargeNumberResult.DONTKNOW;
                }
                if(ConvertToSmallNumber && fNumber == fComparedNumber) return LargeNumberResult.DONTKNOW;
                if (fNumber >= fComparedNumber) { return LargeNumberResult.TRUE; } else return LargeNumberResult.FALSE;
            }
            else return LargeNumberResult.FALSE;
        }
        else { return LargeNumberResult.FALSE; }
    }

    public static void ExtractStringNumber(ref string number, ref float fNumber)
    {
        for (int i = number.Length - 1; i > -1; i--)
        {
            if (!Char.IsDigit(number, i))
            {
                continue;
            }
            else
            {
                fNumber = float.Parse(number.Substring(0, i + 1));
                if (i == (number.Length - 1))
                {
                    number = "";
                }
                else number = number.Substring(i + 1);
                break;
            }
        }
    }

    // DungDA: - Convert from 1xx.xx to 0.1x A etc
    public static string ConvertNumber(string numberConvert)
    {
        string number = numberConvert;
        bool NumConverted = false;
        float NumberHolder = 0;
        ExtractStringNumber(ref number, ref NumberHolder);
        while (NumberHolder >= 10)
        {
            if (number == "zz")
            {
                Debug.Log("Max number can't convert anymore");
                break;
            }
            NumConverted = true;
            NumberHolder /= 1000;
            if (number == "")
            {
                number = "a";
                continue;
            }
            if (number.Length == 1)
            {
                if (number[0] == 'z')
                {
                    number = "aa";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(number);
                    sb[0]++;
                    number = sb.ToString();
                }
            }
            else
            {
                if (number[1] == 'z')
                {
                    StringBuilder sb = new StringBuilder(number);
                    sb[0]++;
                    sb[1] = 'a';
                    number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(number);
                    sb[1]++;
                    number = sb.ToString();
                }
            }
        }

        if (NumConverted == true)
        {
            return NumberHolder.ToString("F2") + number;
        }
        else return numberConvert;
    }
    public static string ConvertToSmalllerNumber(float fNumber,string number)
    {
        if(number=="") return fNumber.ToString("F2") + number;
        while (fNumber < 1)
        {
            if (number.Length == 1)
            {
                    fNumber = fNumber * 1000;
                    if (number[0] == 'a')
                    {
                        number = "";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(number);
                        sb[0]--;
                        number = sb.ToString();
                    }
                
            }
            if (number.Length == 2)
            {
                    fNumber = fNumber * 1000;
                    if (number[1] == 'a')
                    {
                        if (number[0] == 'a')
                        {
                            number = "z";
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder(number);
                            sb[0]--;
                            sb[1] = 'z';
                            number = sb.ToString();
                        }
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(number);
                        sb[1]--;
                        number = sb.ToString();
                    }
                
            }
        }
        return fNumber.ToString("F2") + number;
    }

    private static bool SameStringTail(string number, string comparedNumber)
    {
        float fNumber = 0;
        float fComparedNumber = 0;

        ExtractStringNumber(ref number, ref fNumber);
        ExtractStringNumber(ref comparedNumber, ref fComparedNumber);

        if (number == comparedNumber)
        {
            return true;
        }
        return false;
    }
    public static float Division(string Number, string DevidedNumber)
    {
        float rate = 0;
        int Diff = 0;
        String sNumber = Number;
        String sDevidedNumber = DevidedNumber;
        float fNumber = 0;
        float fDevidedNumber = 0;
        ExtractStringNumber(ref sNumber, ref fNumber);
        ExtractStringNumber(ref sDevidedNumber, ref fDevidedNumber);
        if (sNumber == sDevidedNumber)
        {
            rate = fNumber / fDevidedNumber;
            return rate;
        }
        else
        {
            if (sNumber.Length == 1 && sDevidedNumber.Length == 1)
            {
                Diff = sNumber[0] - sDevidedNumber[0];
            }
            if (sNumber.Length == 2 && sDevidedNumber.Length == 2 && sNumber[0] == sDevidedNumber[0])
            {
                Diff = sNumber[1] - sDevidedNumber[1];
            }
            if(sNumber.Length ==1 && sDevidedNumber.Length==0)
            {
                Diff = ((int)sNumber[0]) - 96;
            }
            rate = fNumber / fDevidedNumber;
            for (int i = 0; i < Diff; i++)
            {
                rate *= 1000;
            }
        }
        return rate;
    }
    public static string ConvertToRoundNumber(string number)
    {
        float fnumber = 0;
        ExtractStringNumber(ref number,ref fnumber);
        return ConvertToSmalllerNumber(fnumber,number);
    }
}

public enum LargeNumberResult
{
    TRUE,
    FALSE,
    DONTKNOW,
}