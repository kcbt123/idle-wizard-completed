using System.Collections;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Text.RegularExpressions;
[CreateAssetMenu(fileName = "LargeNumberUtil", menuName = "Component/LargeNumber", order = 0)]
public class LargeNumberUtil : ScriptableObject
{
    public string TestNumber;
    public string TestNumber2;
    public string Addtion(string Number, string AddedNumber)
    {
        if (!CompareNumber(Number, AddedNumber))
        {
            return Addtion(AddedNumber, Number);
        }
        float fNumber = 0;
        float fAddedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref AddedNumber, ref fAddedNumber);
        Debug.Log("Number: "+Number);
        Debug.Log("AddedNumber: "+AddedNumber);
        if (Number == AddedNumber)
        {
            fNumber += fAddedNumber;
            while (fNumber > 1000)
            {
                fNumber = fNumber / 1000;
                if (Number == "") return fNumber.ToString("F2") + " a";
                if (Number.Length == 1)
                {
                    if (Number[0] == 'z')
                    {
                        Number = "aa";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]++;
                        Number = sb.ToString();
                    }
                }
                else
                {
                    if (Number[1] == 'z')
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]++;
                        sb[1] = 'a';
                        Number = sb.ToString();
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[1]++;
                        Number = sb.ToString();
                    }
                }
            }
        }
        return fNumber.ToString("F2") + Number;
    }

    public string Subtraction(string Number, string SubtractedNumber)
    {
        if (!CompareNumber(Number, SubtractedNumber))
        {
            return "0";
        }
        float fNumber = 0;
        float fSubtractedNumber = 0;
        if(!SameStringTail(Number,SubtractedNumber)){
            SubtractedNumber= ConvertNumber(SubtractedNumber);
        }
        Debug.Log(Number);
        Debug.Log("SubtractedNumber: "+SubtractedNumber);
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref SubtractedNumber, ref fSubtractedNumber);
        if (Number == SubtractedNumber)
        {
            if(fNumber==fSubtractedNumber) return "0";
            fNumber -= fSubtractedNumber;
            if (Number.Length == 1)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (Number[0] == 'a')
                    {
                        Number = "";
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[0]--;
                        Number = sb.ToString();
                    }
                }
            }
            if (Number.Length == 2)
            {
                if (fNumber < 1)
                {
                    fNumber = fNumber * 1000;
                    if (Number[1] == 'a')
                    {
                        if (Number[0] == 'a')
                        {
                            Number = "z";
                        }
                        else
                        {
                            StringBuilder sb = new StringBuilder(Number);
                            sb[0]--;
                            sb[1] = 'z';
                            Number = sb.ToString();
                        }
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder(Number);
                        sb[1]--;
                        Number = sb.ToString();
                    }
                }
            }

        }
        if(fNumber==0 && Number==""){
            return "0";
        }
        return fNumber.ToString("F2") + Number;
    }

    public string Multiplication(string Number, float Multiplier)
    {
        float fNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        fNumber *= Multiplier;
        while (fNumber > 1000)
        {
            fNumber = fNumber / 1000;
            if (Number == "")
            {
                Number = "a";
                continue;
            }
            if (Number.Length == 1)
            {
                if (Number[0] == 'z')
                {
                    Number = "aa";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    Number = sb.ToString();
                }
            }
            else
            {
                if (Number[1] == 'z')
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    sb[1] = 'a';
                    Number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[1]++;
                    Number = sb.ToString();
                }
            }
        }
        return fNumber.ToString("F2") + Number;
    }
    //Check if Number >= ComparedNumber
    public bool CompareNumber(string Number, string ComparedNumber)
    {
        float fNumber = 0;
        float fComparedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref ComparedNumber, ref fComparedNumber);
        if (Number == "" && ComparedNumber == "")
        {
            if (fNumber > fComparedNumber || fNumber == fComparedNumber) { return true; } else return false;
        }
        if (Number.Length > ComparedNumber.Length)
        {
            return true;
        }
        if (Number.Length == ComparedNumber.Length)
        {
            if (Number[0] > ComparedNumber[0])
            {
                return true;
            }
            if (Number[0] == ComparedNumber[0])
            {
                if (Number.Length == 2)
                {
                    if (Number[1] > ComparedNumber[1]) return true;
                    if (Number[1] < ComparedNumber[1]) return false;
                }
                if (fNumber > fComparedNumber || fNumber == fComparedNumber) { return true; } else return false;
            }
            else return false;
        }
        else { return false; }
    }
    //Extract string Number into float + string Numbertail(e.g AZ)
    public void ExtractStringNumber(ref string Number, ref float fNumber)
    {
        for (int i = Number.Length - 1; i > -1; i--)
        {
            if (!Char.IsDigit(Number, i))
            {
                continue;
            }
            else
            {
                fNumber = float.Parse(Number.Substring(0, i + 1));
                if (i == (Number.Length - 1))
                {
                    Number = "";
                }
                else Number = Number.Substring(i + 1);
                break;
            }
        }
    }
    //Convert from 1xx.xx to 0.1x A etc
 public string ConvertNumber(string NumberConvert)
    {
        string Number = NumberConvert;
        float NumberHolder = 0;
        ExtractStringNumber(ref Number, ref NumberHolder);
        while (NumberHolder >= 10)
        {
            if(Number=="zz"){
            break;
            }
            NumberHolder /= 1000;
            if (Number == "") {
                Number="a";
                continue;
            } 

            if (Number.Length == 1)
            {
                if (Number[0] == 'z')
                {
                    Number = "aa";
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    Number = sb.ToString();
                }
            }
            else
            {
                if (Number[1] == 'z')
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[0]++;
                    sb[1] = 'a';
                    Number = sb.ToString();
                }
                else
                {
                    StringBuilder sb = new StringBuilder(Number);
                    sb[1]++;
                    Number = sb.ToString();
                }
            }
        }
            return NumberHolder.ToString("F2") + Number;
    }
    private bool SameStringTail(string Number, string ComparedNumber){
        float fNumber = 0;
        float fComparedNumber = 0;
        ExtractStringNumber(ref Number, ref fNumber);
        ExtractStringNumber(ref ComparedNumber, ref fComparedNumber);
        if(Number==ComparedNumber){
            return true;
        }
        return false;
    }
    public void print(string Number)
    {
        Debug.Log(Number);
    }
}
