using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants
{
    /** ======= MARK: - Strings ======= */
    public static string LARGE_NUMBER_CANT_CALCULATE = "Can't calculate";
}
