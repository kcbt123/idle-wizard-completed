using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectList
{
    /** ======= MARK: - Game Object Names ======= */
    public static string BOSS_HP_BAR = "Image Boss HP Bar";
    public static string LABEL_CURRENT_STAGE = "Label Current Stage";

    public static string LAYOUT_DIAMOND = "Layout Diamond";
    public static string LAYOUT_GOLD = "Layout Gold";
    public static string LAYOUT_RUBY = "Layout Ruby";

    public static string POPUP_DISMANTLE_SELECTED = "Popup Dismantle Selected";
    public static string CONTAINER_DISMANTLE_RUBY_AMOUNT = "Container Dismantle Ruby Amount";

    public static string LABEL_FAMILLIAR_DETAIL_INFO = "Label Familliar Detail Info";

    public static string PANEL_FAMILLIAR_UPGRADE = "Panel Familliar Upgrade";

    public static string FAMILLIAR_SHOT_INIT_PARENT = "Familliar Shot Pool Parent";
}
