using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class GameOffBonus : MonoBehaviour
{
    public static DateTime LogOutDT;
    private static TimeSpan result;
    private static int BossCount;
    public static GeneralData generalData;
    private static int LogOutTotalSec;
    private static string GoldBonus="";
    private const string defaultGoldBonus = "0.01";
    private static string DiamondBonus = "0";
    public TextMeshProUGUI NotiMes;
    public void Start()
    {
        string data = SaveDataManager.instance.DoLoadData(SaveDataManager.generalFileName);
        if (data != "")
        {
            generalData = JsonUtility.FromJson<GeneralData>(data);
            LogOutDT = Convert.ToDateTime(generalData.logOutTime);
            CaculateLogOutBonus();
        }
        else { 
            generalData = new GeneralData();
            NotiMes.transform.parent.gameObject.SetActive(false);
        }
    }
    public static void SetLogOutDT()
    {
        DateTime theTime = DateTime.Now;
        generalData.logOutTime = theTime.ToString("yyyy-MM-dd HH:mm:ss");
        generalData.currentStage = StageDataManager.instance.currentStage;
        SaveDataManager.instance.SaveGeneralData(generalData);
    }

    public void NotificationMessage (){
         List<string> NoteList = new List<string>();
         string TimeCountMes = "Time passed: "+ result.Days+ " Days " + result.Hours+ " Hours " + result.Minutes+ " Minutes " +result.Seconds+ " Seconds";
         string BossKilledMes = "Kill total: "+ BossCount+" Bosses";
         string GoldBonusMes = "Gold gained: "+ GoldBonus;
         string DiamondBonusMes = "Diamond gained: " + DiamondBonus;

        NoteList.Add(TimeCountMes);
        NoteList.Add(BossKilledMes);
        NoteList.Add(GoldBonusMes);
        NoteList.Add(DiamondBonusMes);
        Debug.Log(NotiMes.text);
        NotiMes.text = "";
        for (int i = 0; i < NoteList.Count; i++)
        {
            NotiMes.text += NoteList[i];
            NotiMes.text += "\n";
        }

    }

    public void CaculateLogOutBonus(){
        LogOutTotalSec = CalculateLogOutSecs(DateTime.Now);
        CaculateGoldBonus();
        NotificationMessage();
    }
    public int CalculateLogOutSecs(DateTime LogIn)
    {
        if (LogOutDT == null) {
            result = LogIn.Subtract(LogIn);
        }else result  = LogIn.Subtract(LogOutDT);
        int seconds = Convert.ToInt32(result.TotalSeconds);
        return seconds;
    }
    //Caculate gold gained and total boss killed
    public static void CaculateGoldBonus(){
        GoldBonus = LargeNumberStaticUtil.Multiplication(defaultGoldBonus, LogOutTotalSec);
    }
    public static void GetLogoffGold()
    {
        CurrencyController.AddGold(GoldBonus);
    }
    void OnApplicationQuit()
    {
        if (SaveDataManager.instance.reset) return;
        SetLogOutDT();
    }

}