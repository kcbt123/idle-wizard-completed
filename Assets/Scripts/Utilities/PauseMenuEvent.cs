using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
public class PauseMenuEvent : MonoBehaviour
{
    public AudioSource AudioSource;
    [SerializeField] Slider VolumeSlider;
    private float MusicVolume;
    private string key;
    private void Start() {
        key = "Volume" + AudioSource.name;
        float Volume = 1f;
        if(PlayerPrefs.HasKey(key))
        {
            Volume = PlayerPrefs.GetFloat(key);
        }
        AudioSource.volume = Volume;
        VolumeSlider.value = Volume;
        Debug.Log(AudioSource.name);
    }
    // public void ReturnMainMenu()
    // {
    //     SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    //     GameOffBonus.SetLogOutDT();
    // }
    public void QuitGame()
    {
        Debug.Log("Quit Game");
        Application.Quit();
    }
    public void ChangeVolume()
    {
        PlayerPrefs.SetFloat(key, VolumeSlider.value);
        MusicVolume = VolumeSlider.value;
        AudioSource.volume = VolumeSlider.value;
        if(AudioSource.gameObject.name== "Wizard Shot SFX Source")
        {
            AudioSource.transform.GetComponents<AudioSource>()[0].volume = VolumeSlider.value;
            AudioSource.transform.GetComponents<AudioSource>()[1].volume = VolumeSlider.value;
        }
    }
}
