using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TabButton : MonoBehaviour ,IPointerClickHandler, IPointerEnterHandler,IPointerExitHandler
{
    private bool isActive= false;
    private Image image;
    private int tabIndex;
    private TabController controller;
    private void Awake() {
        image = GetComponent<Image>();
        controller =FindObjectOfType<TabController>();
    }
    public void setIndex (int index){
        tabIndex = index;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        controller.ButtonMouseClick(tabIndex);
        SoundManager.instance.buttonSFXSource.PlayOneShot(SoundManager.instance.soundList.buttonSFX);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(!isActive){
            image.color = Color.clear;
        }
        controller.ButtonMouseEnter(tabIndex);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(!isActive){
            image.color = Color.white;
        }
        controller.ButtonMouseExit(tabIndex);
    }

    public void ToggleActive(){
        isActive=!isActive;
        if(isActive){
            image.color = Color.yellow;
        } else image.color = Color.white;
    }
}
