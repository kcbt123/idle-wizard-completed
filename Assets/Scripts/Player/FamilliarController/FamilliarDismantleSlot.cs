using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
public class FamilliarDismantleSlot : MonoBehaviour ,IPointerClickHandler
{
    public bool Selected = false;
    public bool Locked = false;
    public Image Familliar;
    public Image Lock;
    private int  childIndex;
    public TextMeshProUGUI familliarDetails;
    // Start is called before the first frame update
    void Start()
    {
        childIndex = transform.parent.gameObject.transform.parent.transform.GetSiblingIndex();
        familliarDetails = GameObject.Find(GameObjectList.LABEL_FAMILLIAR_DETAIL_INFO).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnPointerClick(PointerEventData eventData)
    {
        familliarDetails.text = "<align=center>";
        familliarDetails.text += "<b><#0000FF>";
        familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].name;
        familliarDetails.text += "</b></align>\n<color=#005500>";
        familliarDetails.text += "Base Damage: <#0000FF>" + FamilliarDismantleController.instance.familliarInventory[childIndex].stats.baseDamage;
        familliarDetails.text += "\n<color=#005500>";
        familliarDetails.text += "Crit Rate: <#0000FF>" + FamilliarDismantleController.instance.familliarInventory[childIndex].stats.CRTRate+"%";
        familliarDetails.text += "\n<color=#005500>";
        for(int i=0;i< FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills.Count; i++)
        {
            familliarDetails.text += "Rank ";
            familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills[i].requiredRank+": ";
            familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills[i].name;
            familliarDetails.text += "\n<#0000FF>";
            familliarDetails.text += FamilliarDismantleController.instance.familliarInventory[childIndex].passiveSkills[i].effectDesc;
            familliarDetails.text += "\n<color=#005500>";
            familliarDetails.text += "\n";
        }
    }
    public void ToggleLock()
    {
        if (!Locked)
        {
            if (Selected)
            {
                UnSelect();
            }
            LockSlot();
        }
        else
        {
            UnlockSlot();
        }
        FamilliarDismantleController.instance.familliarInventory[childIndex].currentStats.unlocked = !Locked;
        SaveDataManager.instance.SaveFamilliarData_Overwrite(FamilliarDismantleController.instance.familliarInventory);
    }
    public void ToggleSelect()
    {
        if (!Selected)
        {
            Select();
        }
        else
        {
            UnSelect();
        }
    }
    public void Select(){
        if (!Selected)
        {
            Selected = true;
            FamilliarDismantleController.instance.TotalSelected++;
            Familliar.color = new Color32(100, 100, 255, 255);
            FamilliarDismantleController.instance.dismantleListIndex.Add(childIndex);
        }
    }
    public void UnSelect(){
        if (Selected)
        {
            Selected = false;
            Familliar.color = new Color32(255, 255, 255, 255);
            FamilliarDismantleController.instance.TotalSelected--;
            FamilliarDismantleController.instance.dismantleListIndex.Remove(childIndex);
        }

    }
    public void LockSlot()
    {
        if (!Locked)
        {
            Locked = true;
            Lock.sprite = Resources.Load<Sprite>("Sprites/UI/padlock");
        }
    }
    public void UnlockSlot()
    {
        if (Locked)
        {
            Locked = false;
            Lock.sprite = Resources.Load<Sprite>("Sprites/UI/unlock");
        }
    }
    public void SelectDismantle()
    {
        if (Locked) return;
        ToggleSelect();
    }
}
