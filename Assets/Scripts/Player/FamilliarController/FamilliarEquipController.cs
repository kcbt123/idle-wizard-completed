using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public  class FamilliarEquipController : MonoBehaviour
{
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private FamilliarEquipController()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static FamilliarEquipController _instance;

    public static FamilliarEquipController instance
    {
        get
        {
            return _instance;
        }
    }

    // Start is called before the first frame update
    public List<int> number = new List<int>();
    public int currentPage = 0;
    public int maxPage = 0;
    public List<FamilliarData> equipFamilliar = new List<FamilliarData>();
    public List<int> equipedList;
    private GameObject familliarInventory;
    private List<EventListener> _eventListeners;

    void Start()
    {
        AddListeners();
        equipedList = SaveDataManager.instance.LoadEquippedFamilliarData();
        if (equipedList.Count == 0)
        {
            equipedList = new List<int>(new int[] { -1, -1,-1, -1 });
        }
        equipFamilliar = SaveDataManager.instance.LoadFamilliarInventoryData();
        SetUpFamilliarInventory();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Nextpage()
    {
        if(currentPage < maxPage)
        {
            currentPage++;
            SetUpFamilliarInventory();
        }
    }
    public void PrevPage()
    {
        if (currentPage > 0)
        {
            currentPage--;
            SetUpFamilliarInventory();
        }

    }
    public void SetUpFamilliarInventory()
    {
        if (equipedList.Count == 0) return;
        for (int i = 0; i < 5; i++)
        {
            instance.familliarInventory.transform.GetChild(i).gameObject.SetActive(true);
            if (i + instance.currentPage * 5 > instance.equipFamilliar.Count - 1)
            {
                instance.familliarInventory.transform.GetChild(i).gameObject.SetActive(false);
                continue;
            }
            Transform familliar = instance.familliarInventory.transform.GetChild(i).transform.GetChild(0);
            for (int j = 0; j < 5; j++)
            {
                familliar.GetChild(5).transform.GetChild(j).gameObject.SetActive(false);
            }
            familliar.GetChild(4).gameObject.SetActive(false);
            if (instance.currentPage == 0)
            {
                for (int j = 0; j < 4; j++)
                {
                    if ((5 * instance.currentPage + i) == instance.equipedList[j])
                    {
                        familliar.GetChild(4).gameObject.SetActive(true);
                    }
                }
            }
            string damage = equipFamilliar[currentPage * 5 + i].currentStats.shotDamage;
            if (damage == "") damage = equipFamilliar[currentPage * 5 + i].stats.baseDamage;

            familliar.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(FamilliarDismantleController.instance.GetRarityImage(equipFamilliar[currentPage * 5 + i].rarity));
            familliar.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>(equipFamilliar[currentPage * 5 + i].image);
            familliar.GetChild(2).GetComponent<TextMeshProUGUI>().text = equipFamilliar[currentPage * 5 + i].name;
            familliar.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Damage:" + damage;
            for (int j = 0; j < equipFamilliar[currentPage * 5 + i].currentStats.rank; j++)
            {
                familliar.GetChild(5).transform.GetChild(j).gameObject.SetActive(true);
            }
        }
        if (GameObject.Find(GameObjectList.PANEL_FAMILLIAR_UPGRADE) !=null) SetUpAllUpgradeFamilliarUI();
        for (int i = 0; i < 4; i++)
        {
            Transform equipSlot = instance.familliarInventory.transform.parent.GetChild(i);
            for (int j = 0; j < 5; j++)
            {
                equipSlot.GetChild(3).transform.GetChild(j).gameObject.SetActive(false);
            }
            if (instance.equipedList[i] < 0)
            {
                SetBlank(equipSlot);
                continue;
            }
            equipSlot.GetChild(1).transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(FamilliarDismantleController.instance.GetRarityImage(equipFamilliar[equipedList[i]].rarity));
            equipSlot.GetChild(1).transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>(instance.equipFamilliar[instance.equipedList[i]].image);
            equipSlot.GetChild(2).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = instance.equipFamilliar[instance.equipedList[i]].name;
            equipSlot.GetChild(2).transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Level: "+ equipFamilliar[instance.equipedList[i]].currentStats.level;
            string damage = equipFamilliar[instance.equipedList[i]].currentStats.shotDamage;
            if (damage == "") damage = equipFamilliar[instance.equipedList[i]].stats.baseDamage;
            equipSlot.GetChild(2).transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Damage: "+ damage;
            equipSlot.GetChild(2).transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Rarity: " + equipFamilliar[instance.equipedList[i]].rarity;
            for (int j = 0; j < equipFamilliar[instance.equipedList[i]].currentStats.rank; j++)
            {
                equipSlot.GetChild(3).transform.GetChild(j).gameObject.SetActive(true);
            }
        }
    }
    public  void InitFamilliarInventory()
    {
        familliarInventory = transform.gameObject;
        equipFamilliar = SaveDataManager.instance.LoadFamilliarInventoryData();
        maxPage = (int)Math.Ceiling((double)equipFamilliar.Count / 5) - 1;
        currentPage = 0;
        equipedList = SaveDataManager.instance.LoadEquippedFamilliarData();
        if (equipedList.Count == 0)
        {
            for (int i = 0; i < 4; i++)
            {
                equipedList.Add((int)-1);
            }
        }
        SetUpFamilliarInventory();
    }

    public void SortFamilliarList()
    {
        List<FamilliarData> sortedList = equipFamilliar;
        int count=0;

        for (int j = 0; j < 4; j++)
        {
            int smallestPos = j;
            if (equipedList[j] >= 0)
            {
                for (int i = j + 1; i < 4; i++)
                {
                    if (equipedList[i] < 0) continue;
                    if (equipedList[i] < equipedList[smallestPos])
                    {
                        smallestPos = i;
                    }
                }
                if (smallestPos != j)
                {
                    FamilliarData temp = sortedList[equipedList[j]];
                    sortedList[equipedList[j]] = sortedList[equipedList[smallestPos]];
                    sortedList[equipedList[smallestPos]] = temp;
                    int iTemp = equipedList[j];
                    equipedList[j] = equipedList[smallestPos];
                    equipedList[smallestPos] = iTemp;
                }
            }
        }

        for (int j = 0; j < 4; j++)
        {
            if (equipedList[j] >= 0)
            {
                for (int i = 0; i < equipFamilliar.Count; i++)
                {
                    if (equipedList[j] == i)
                    {
                        FamilliarData temp = sortedList[count];
                        sortedList[count] = sortedList[i];
                        sortedList[i] = temp;
                        equipedList[j] = count;
                        break;
                    }
                }
                sortedList[count].currentStats.unlocked = false;
                count++;
            }
        }

        SaveDataManager.instance.SaveFamilliarData_Overwrite(sortedList);
        SaveDataManager.instance.SaveEquippedFamilliar(equipedList);
        SetUpFamilliarInventory();
    }
    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
           CustomEventSystem.instance.AddListener(EventCode.ON_EQUIP_FAMILLIAR, this, OnEquipFamilliar),
           CustomEventSystem.instance.AddListener(EventCode.ON_UNEQUIP_FAMILLIAR, this, OnUnEquipFamilliar),
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }
    public void OnEquipFamilliar(object[] eventParam)
    {
        
    }
    public void OnUnEquipFamilliar(object[] eventParam)
    {
       
    }
    private void SetBlank(Transform familliar)
    {
        familliar.GetChild(1).transform.GetChild(0).GetComponent<Image>().sprite = null;
        familliar.GetChild(1).transform.GetChild(1).GetComponent<Image>().sprite = null;
        familliar.GetChild(2).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Name";
        familliar.GetChild(2).transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Level: ";
        familliar.GetChild(2).transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "Damage: ";
        familliar.GetChild(2).transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = "Rarity: ";
    }
    private void SetUpgradeFamilliarUI(Transform UpgradeSlot, int i)
    {
        Transform upgradeFamilliarImage = UpgradeSlot.GetChild(0).transform.GetChild(0);
        Transform upgradeFamilliarDes = UpgradeSlot.GetChild(0).transform.GetChild(1).transform;
        for(int j = 0; j < 5; j++)
        {
            upgradeFamilliarImage.transform.GetChild(2).transform.GetChild(j).gameObject.SetActive(false);
        }
        for(int j = 1; j < 4; j++)
        {
            UpgradeSlot.GetChild(j).transform.GetChild(3).gameObject.SetActive(true);
        }
        if (equipedList[i] >= 0)
        {
            upgradeFamilliarImage.transform.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(FamilliarDismantleController.instance.GetRarityImage(equipFamilliar[equipedList[i]].rarity));
            upgradeFamilliarImage.transform.GetChild(1).GetComponent<Image>().sprite = Resources.Load<Sprite>(equipFamilliar[equipedList[i]].image);
            for(int j=0;j< equipFamilliar[equipedList[i]].currentStats.rank; j++)
            {
                upgradeFamilliarImage.transform.GetChild(2).transform.GetChild(j).gameObject.SetActive(true);
                if(j == 0) UpgradeSlot.GetChild(1).transform.GetChild(3).gameObject.SetActive(false);
                if(j == 2) UpgradeSlot.GetChild(2).transform.GetChild(3).gameObject.SetActive(false);
                if(j == 4) UpgradeSlot.GetChild(3).transform.GetChild(3).gameObject.SetActive(false);
            }
            upgradeFamilliarDes.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Name: " + equipFamilliar[equipedList[i]].name;
            upgradeFamilliarDes.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Level: " + equipFamilliar[equipedList[i]].currentStats.level;
        }
        else
        {
            upgradeFamilliarImage.transform.GetChild(0).GetComponent<Image>().sprite = null;
            upgradeFamilliarImage.transform.GetChild(1).GetComponent<Image>().sprite = null;
            upgradeFamilliarDes.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Name: ";
            upgradeFamilliarDes.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Level: ";
        }
    }
    public void SetUpAllUpgradeFamilliarUI()
    {
        Transform familliarUpgradePanel = GameObject.Find(GameObjectList.PANEL_FAMILLIAR_UPGRADE).transform.GetChild(0).transform;
        for(int i = 0; i < 4; i++)
        {
            SetUpgradeFamilliarUI(familliarUpgradePanel.GetChild(i).transform, i);
        }
    }
}
