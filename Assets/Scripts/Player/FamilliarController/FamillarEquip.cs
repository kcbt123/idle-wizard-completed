using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class FamillarEquip : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    [SerializeField] private Canvas canvas;
    private RectTransform rectTransform;
    private CanvasGroup CanvasGroup;
    private Vector3 InitPos;
    private void Awake()
    {
        rectTransform = this.GetComponent<RectTransform>();
        InitPos = rectTransform.anchoredPosition;
        //Debug.Log(InitPos);
        CanvasGroup = GetComponent<CanvasGroup>();
    }
    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");
        CanvasGroup.alpha = 0.6f;
        CanvasGroup.blocksRaycasts = false;
    }
    public void OnDrag(PointerEventData eventData)
    {
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        CanvasGroup.alpha = 1f;
        CanvasGroup.blocksRaycasts = true;
        rectTransform.anchoredPosition = InitPos;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDown");
    }


}
