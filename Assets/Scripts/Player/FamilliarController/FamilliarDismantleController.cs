using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class FamilliarDismantleController : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private FamilliarDismantleController()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static FamilliarDismantleController _instance;

    public static FamilliarDismantleController instance
    {
        get
        {
            return _instance;
        }
    }
    // Start is called before the first frame update
    public int TotalSelected = 0;
    public GameObject Prefab;
    private int LineCount;
    public List<FamilliarData> familliarInventory;
    public List<int> dismantleListIndex;
    public TextMeshProUGUI dismantle;
    private List<EventListener> _eventListeners;
    private const string rRubyDismantle = "5";
    private const string sRRubyDismantle = "10";
    private const string sSRRubyDismantle = "20";
    private string totalRubyDismantle = "0";
    public GameObject familliarUpgrade;

    private List<int> gridCellSize = new List<int>{ 50, 50 };
    private List<int> gridSpacing = new List<int>{ 80, 100 };

    void Start()
    {
        AddListeners();
        ResetDismantleList();
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void AddFamilliar(FamilliarData data)
    {
        //Prefab = Resources.Load("FamilliarSlot") as GameObject;
        var copy = Instantiate(Prefab);
        UpdateSlot(copy.gameObject, data);
        copy.transform.SetParent(transform);
    }
    public void ResetDismantleList(){
        familliarInventory = SaveDataManager.instance.LoadFamilliarInventoryData();
        dismantleListIndex = new List<int>();
        for (int i=0; i <familliarInventory.Count; i++)
        {
            if( i < transform.childCount)
            {
                UpdateSlot(transform.GetChild(i).gameObject, familliarInventory[i]);
            }
            else { AddFamilliar(familliarInventory[i]); }

            List<int> equipedList = SaveDataManager.instance.LoadEquippedFamilliarData();
            //if (equipedList.Exists(element => element == i))
            //{
            //    Transform slot = transform.GetChild(i).transform.GetChild(0).transform.GetChild(1);
            //    if (!slot.GetComponent<FamilliarDismantleSlot>().Locked)
            //    {
            //        slot.GetComponent<FamilliarDismantleSlot>().ToggleLock();
            //    }
            //}
        }
        dismantleListIndex.Clear();
        FamilliarEquipController.instance.SetUpFamilliarInventory();
        FixSize();
    }
    public void DeleteDismantleList()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    public void DeleteNumberOfChild(int number)
    {
        for(int i = transform.childCount- number; i < transform.childCount; i++) { 
            Destroy(transform.GetChild(i).gameObject);
        }
    }
    private void FixSize(){
        LineCount = (int)Math.Ceiling((double)transform.childCount / 5);

        // HieuBT: - Calculate new Grid Height
        int rowCount = familliarInventory.Count / 5;
        int rowExcess = familliarInventory.Count % 5;

        int pendingHeight = gridCellSize[1] * rowCount + gridSpacing[1] * (rowCount - 1);
        if (rowExcess > 0)
        {
            pendingHeight += gridCellSize[1] + gridSpacing[1];
        }

        pendingHeight += 100;

        //if (LineCount > 2)
        //{
        //    RectTransform dismantleInventory = transform.GetComponent<RectTransform>();
        //    dismantleInventory.sizeDelta = new Vector2(pendingWidth, 300);
        //}
        //else
        //{
        //    RectTransform dismantleInventory = transform.GetComponent<RectTransform>();
        //    dismantleInventory.sizeDelta = new Vector2(pendingWidth, 300);
        //}

        RectTransform dismantleInventory = transform.GetComponent<RectTransform>();
        dismantleInventory.sizeDelta = new Vector2(750, pendingHeight);
    }
    public void DismantleSelected()
    {
        foreach (int index in dismantleListIndex.OrderByDescending(v => v))
        {
            familliarInventory.RemoveAt(index);
            if (FamilliarEquipController.instance.equipedList.Contains(index))
            {
                FamilliarEquipController.instance.equipedList[FamilliarEquipController.instance.equipedList.IndexOf(index)] = -1;
            }
        }
        FamilliarEquipController.instance.SortFamilliarList();
        CustomEventSystem.instance.DispatchEvent(EventCode.ON_UNEQUIP_FAMILLIAR, new object[] {

        });
        //Add ruby to save data
        CurrencyController.AddRuby(totalRubyDismantle);
        //Update inventory list
        SaveDataManager.instance.SaveFamilliarData_Overwrite(familliarInventory);
        SaveDataManager.instance.SaveEquippedFamilliar(FamilliarEquipController.instance.equipedList);
        dismantleListIndex.Clear();
        DeleteNumberOfChild(TotalSelected);
        Debug.Log("Child:    " + transform.childCount);
        ResetDismantleList();
        TotalSelected = 0;
    }
    public void SelectAllFamilliar()
    {
        for (int i = 0; i < familliarInventory.Count; i++)
        {
            if (!transform.GetChild(i).transform.GetChild(0).transform.GetChild(1).GetComponent<FamilliarDismantleSlot>().Locked)
            {
                transform.GetChild(i).transform.GetChild(0).transform.GetChild(1).GetComponent<FamilliarDismantleSlot>().Select();
            }
        }
    }
    private void UpdateSlot(GameObject slot , FamilliarData data)
    {
        Transform rarityImage = slot.transform.GetChild(0).transform.GetChild(0);
        Transform familliarImage = slot.transform.GetChild(0).transform.GetChild(1);

        rarityImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(GetRarityImage(data.rarity));
        familliarImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(data.image);

        Transform dismantleSlot = slot.transform.GetChild(0).transform.GetChild(1);
        dismantleSlot.GetComponent<FamilliarDismantleSlot>().UnSelect();

        if (dismantleSlot.GetComponent<FamilliarDismantleSlot>().Locked)
        {
            dismantleSlot.GetComponent<FamilliarDismantleSlot>().UnlockSlot();
        }
        if (!data.currentStats.unlocked)
        {
                dismantleSlot.GetComponent<FamilliarDismantleSlot>().LockSlot();
        }

        for (int i = 0; i < 5; i++)
        {
            slot.transform.GetChild(0).transform.GetChild(2).transform.GetChild(i).gameObject.SetActive(false);
        }
        for (int i=0;i< data.currentStats.rank; i++)
        {
            slot.transform.GetChild(0).transform.GetChild(2).transform.GetChild(i).gameObject.SetActive(true);
        }
    }
    public void SetNotice()
    {
        totalRubyDismantle = "0";
        dismantle = GameObject.Find(GameObjectList.POPUP_DISMANTLE_SELECTED).transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        dismantle.text = "Dismantle " + dismantleListIndex.Count + " familliars";
        foreach (int index in dismantleListIndex.OrderByDescending(v => v))
        {
            totalRubyDismantle = LargeNumberStaticUtil.Addition(totalRubyDismantle, GetRubyDismantle(familliarInventory[index].rarity));
        }
        GameObject.Find(GameObjectList.CONTAINER_DISMANTLE_RUBY_AMOUNT).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "Total: " + totalRubyDismantle;
    }
    public string GetRarityImage(string rarity)
    {
        string rarityImageString = "";
        switch (rarity)
        {
            case "R":
                rarityImageString = "Sprites/UI/Frames/Silver_square";
                break;
            case "SR":
                rarityImageString = "Sprites/UI/Frames/Gold_square";
                break;
            case "SSR":
                rarityImageString = "Sprites/UI/Frames/Rainbow Square";
                break;
        }
        return rarityImageString;
    }
    public void SetRankUI(GameObject RankUI, int currentRank)
    {
        for(int i=0;i< currentRank - 1; i++)
        {

        }
    }
    public string GetRubyDismantle(string rarity)
    {
        switch (rarity)
        {
            case "R":
                return rRubyDismantle;
            case "SR":
                return sRRubyDismantle;
            case "SSR":
                return sSRRubyDismantle;
        }
        return "";
    }
    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
           CustomEventSystem.instance.AddListener(EventCode.ON_EQUIP_FAMILLIAR, this, OnEquipFamilliar),
           CustomEventSystem.instance.AddListener(EventCode.ON_UNEQUIP_FAMILLIAR, this, OnUnEquipFamilliar),
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }
    public void OnEquipFamilliar(object[] eventParam)
    {

    }
    public void OnUnEquipFamilliar(object[] eventParam)
    {

    }
}
