using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class FamillarEquipSlot : MonoBehaviour, IDropHandler
{
    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");
        if (eventData.pointerDrag != null)
        {
            Debug.Log(eventData.pointerDrag.tag);
            if (eventData.pointerDrag.tag == "EquipedImage")
            {
                if(transform.name == "Panel Slider Familliar")
                {
                    UnEquip(eventData.pointerDrag.transform.parent.transform.GetSiblingIndex());
                }
                return;
            }
            if (transform.GetSiblingIndex() < 4)
            {
                int familliarEqipedIndex = eventData.pointerDrag.transform.parent.transform.GetSiblingIndex() + FamilliarEquipController.instance.currentPage * 5;
                if (FamilliarEquipController.instance.equipedList.Exists(element => element == familliarEqipedIndex)) return;
                EquipFamilliar(transform.GetSiblingIndex(), familliarEqipedIndex);
            }
        }
    }

    private void EquipFamilliar( int pos , int inventoryIndex)
    {
        SoundManager.instance.buttonSFXSource.PlayOneShot(SoundManager.instance.soundList.buttonSFX);
        if (FamilliarEquipController.instance.equipedList[pos] >= 0)
        {
            FamilliarEquipController.instance.equipFamilliar[FamilliarEquipController.instance.equipedList[pos]].currentStats.unlocked = true;
        }
        FamilliarEquipController.instance.equipedList[pos] = inventoryIndex;
        FamilliarEquipController.instance.SortFamilliarList();
        CustomEventSystem.instance.DispatchEvent(EventCode.ON_EQUIP_FAMILLIAR, new object[] {

        });
    }
    private void UnEquip(int pos)
    {
        SoundManager.instance.buttonSFXSource.PlayOneShot(SoundManager.instance.soundList.buttonSFX);
        FamilliarEquipController.instance.equipFamilliar[FamilliarEquipController.instance.equipedList[pos]].currentStats.unlocked = true;
        FamilliarEquipController.instance.equipedList[pos] = -1;
        FamilliarEquipController.instance.SortFamilliarList();
        CustomEventSystem.instance.DispatchEvent(EventCode.ON_UNEQUIP_FAMILLIAR, new object[] {
            
        });
    }
}
