using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class FamilliarRankUpController : MonoBehaviour
{
    public static int totalRankUp = 0;
    public GameObject Prefab;
    public static List<int> similarFamilliarIndex= new List<int>();
    public static List<int> rankupContribute = new List<int>();
    private int LineCount;
    private int rankupIndex;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GetSimilarFamilliar(int index)
    {
        similarFamilliarIndex.Clear();
        DeleteRankupList();
        index = FamilliarEquipController.instance.equipedList[index];
        if (index < 0) return;
        totalRankUp = FamilliarEquipController.instance.equipFamilliar[index].currentStats.rank;
        rankupIndex = index;
        for (int i=0;i< FamilliarEquipController.instance.equipFamilliar.Count; i++)
        {
            if (i == index || FamilliarEquipController.instance.equipedList.Contains(i)) continue;
            if(FamilliarEquipController.instance.equipFamilliar[i].id == FamilliarEquipController.instance.equipFamilliar[index].id)
            {
                similarFamilliarIndex.Add(i);
            }
        }
        for(int i=0;i< similarFamilliarIndex.Count; i++)
        {
            var copy = Instantiate(Prefab);
            UpdateSlot(copy.gameObject, FamilliarEquipController.instance.equipFamilliar[similarFamilliarIndex[i]]);
            copy.transform.parent = transform;
        }
        FixSize();
    }
    private void UpdateSlot(GameObject slot, FamilliarData data)
    {
        Transform rarityImage = slot.transform.GetChild(0);
        Transform familliarImage = slot.transform.GetChild(1);
        rarityImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(FamilliarDismantleController.instance.GetRarityImage(data.rarity));
        familliarImage.GetComponent<Image>().sprite = Resources.Load<Sprite>(data.image);
        for(int i=0;i< data.currentStats.rank; i++)
        {
            slot.transform.GetChild(2).transform.GetChild(i).gameObject.SetActive(true);
        }
    }
    public void DeleteRankupList()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    private void FixSize()
    {
        LineCount = (int)Math.Ceiling((double)similarFamilliarIndex.Count / 5);
        //Debug.Log("XXXXXXX" + similarFamilliarIndex.Count);
        //Debug.Log("XXXXXXX" + LineCount);
        if (LineCount > 1)
        {
            RectTransform rankupInventory = transform.GetComponent<RectTransform>();
            rankupInventory.sizeDelta = new Vector2(717, 110 * LineCount);
        }
        else
        {
            RectTransform rankupInventory = transform.GetComponent<RectTransform>();
            rankupInventory.sizeDelta = new Vector2(717, 200);
        }
    }
    
    public static void AddToContributeList(int pos)
    {
        rankupContribute.Add(similarFamilliarIndex[pos]);
    }
    public static void RemoveFromContributeList(int pos)
    {
        rankupContribute.Remove(similarFamilliarIndex[pos]);
    }
    public void RankUp()
    {
        foreach (int index in rankupContribute.OrderByDescending(v => v))
        {
            FamilliarEquipController.instance.equipFamilliar.RemoveAt(index);
        }
        FamilliarEquipController.instance.equipFamilliar[rankupIndex].currentStats.rank = totalRankUp;
        //Update inventory list
        SaveDataManager.instance.SaveFamilliarData_Overwrite(FamilliarEquipController.instance.equipFamilliar);
        FamilliarEquipController.instance.SetUpAllUpgradeFamilliarUI();
        rankupContribute.Clear();
    }
}
