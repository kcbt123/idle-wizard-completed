using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BackgroundNextStageController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    public Transform movedObject;
    public float moveDistance = 0.5f;
    public float moveDuration = 0.5f;
    public Ease animEse;

    public bool isCameraMoveRunning;

    /** ======= MARK: - Functions ======= */

    private void Start()
    {
        isCameraMoveRunning = false;
    }

    public void MoveCamera()
    {
        StartCoroutine(MoveBackground());
    }

    IEnumerator MoveBackground()
    {
        isCameraMoveRunning = true;
        float newPos = movedObject.transform.position.y + moveDistance;
        movedObject.transform.DOMoveY(newPos, moveDuration).SetEase(animEse);
        yield return new WaitForSeconds(moveDuration);
        isCameraMoveRunning = false;
    }
}
