using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectPlayer : MonoBehaviour
{
    public void PlayButtonSound()
    {
        SoundManager.instance.buttonSFXSource.PlayOneShot(SoundManager.instance.soundList.buttonSFX);
    }    
}
