using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GUIGameplay : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private List<EventListener> _eventListeners;

    [SerializeField]
    private TextMeshProUGUI timerLabel;

    [SerializeField]
    private TextMeshProUGUI currentStageLabel;

    [SerializeField]
    private GameObject bossChallengeButton;

    [SerializeField]
    private GameObject monsterObject;

    float currentTime = 30f;
    private bool isMovedToLoopStage = false;

    /** ======= MARK: - MonoBehaviour Methods ======= */
    void Start()
    {
        AddListeners();
        bossChallengeButton.SetActive(false);

        if (StageDataManager.instance.currentStage % 5 == 1)
        {
            StopTimerLabel();
        }
    }

    void Update()
    {
        if (!isMovedToLoopStage)
        {
            if (StageDataManager.instance.currentStage % 5 == 0
                && currentTime <= 0f
                && timerLabel.text == "00.00"
                && monsterObject.GetComponent<BaseMonsterController>().monsterHP != "0")
            {
                bossChallengeButton.SetActive(true);
                MoveToLoopingStage();
                isMovedToLoopStage = true;
            }
        }
    }

    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {
            CustomEventSystem.instance.AddListener(EventCode.ON_BOSS_DEFEATED, this, MoveToNextStage)
        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }

    /** ======= MARK: - Listeners ======= */

    private void MoveToNextStage(object[] eventParam)
    {
        if (isMovedToLoopStage)
        {
            AddPlayerReward(StageDataManager.instance.currentStage - 1);
            MoveToLoopingStage();
        }
        else
        {
            AddPlayerReward(StageDataManager.instance.currentStage);
            if (StageDataManager.instance.currentStage % 5 == 0)
            {
                StopTimerLabel();
            }
            StartCoroutine(NextStageFlow());
        }
    }

    /** ======= MARK: - Next Stage Functions ======= */

    private void AddPlayerReward(int stageId)
    {
        string addedGold = StageDataManager.instance.CalculateGoldReward(stageId);
        CurrencyController.AddGold(addedGold);
        string addedDiamond = StageDataManager.instance.CalculateBossDiamondReward(stageId);
        CurrencyController.AddDiamond(addedDiamond);
    }

    IEnumerator NextStageFlow()
    {
        yield return StartCoroutine(MoveCamera());
        yield return StartCoroutine(MoveToNextStageLogic());
    }

    IEnumerator MoveCamera()
    {
        monsterObject.SetActive(false);
        GUIManager.instance.gameObject.GetComponent<BackgroundNextStageController>().MoveCamera();
        yield return new WaitForSeconds(GUIManager.instance.gameObject.GetComponent<BackgroundNextStageController>().moveDuration);
    }

    IEnumerator MoveToNextStageLogic()
    {
        // HieuBT: - Update full data cho cai StageDataManager
        StageDataManager.instance.currentStage = StageDataManager.instance.currentStage + 1;
        StageDataManager.instance.currentStageReward = StageDataManager.instance.CalculateGoldReward(StageDataManager.instance.currentStage);
        StageDataManager.instance.currentMonsterHP = StageDataManager.instance.CalculateMonsterHP(StageDataManager.instance.currentStage);

        if (StageDataManager.instance.currentStage % 5 == 0)
        {
            ActivateBossTimer();
        }

        currentStageLabel.text = "Current Stage: " + StageDataManager.instance.currentStage;

        CustomEventSystem.instance.DispatchEvent(EventCode.ON_NEXT_STAGE, new object[] {

        });

        monsterObject.SetActive(true);
        yield return null;
    }

    /** ======= MARK: - Looped Stage Functions ======= */

    public void MoveToLoopingStage()
    {
        // HieuBT: - Current Stage se la stage 5x*
        currentStageLabel.text = "Current Stage: " + StageDataManager.instance.currentStage + "*";

        int loopedStage = StageDataManager.instance.currentStage - 1;

        monsterObject.SetActive(true);

        CustomEventSystem.instance.DispatchEvent(EventCode.ON_NEXT_STAGE, new object[] {
            loopedStage.ToString()
        });

        timerLabel.gameObject.SetActive(false);
        currentTime = 0f;
    }

    /** ======= MARK: - Boss Challenge Function ======= */

    public void ChallengeBossAgain()
    {
        // HieuBT: - Current Stage se la stage 5x
        bossChallengeButton.SetActive(false);
        isMovedToLoopStage = false;

        currentStageLabel.text = "Current Stage: " + StageDataManager.instance.currentStage;
        ActivateBossTimer();
  
        monsterObject.SetActive(true);

        CustomEventSystem.instance.DispatchEvent(EventCode.ON_NEXT_STAGE, new object[] {

        });
    }

    /** ======= MARK: - Handle Boss Timer ======= */

    public void ActivateBossTimer()
    {
        timerLabel.gameObject.SetActive(true);
        currentTime = 30f;
        timerLabel.text = "30.00";
        StartCoroutine(UpdateTimer());
    }

    private bool isTimerToZero = new bool();
    IEnumerator UpdateTimer()
    {
        isTimerToZero = false;

        while (!isTimerToZero)
        {
            UpdateTimerLabel();
            yield return null;
        }
    }
    public void UpdateTimerLabel()
    {
        if (currentTime - Time.deltaTime <= 0f)
        {
            currentTime = 0f;
            timerLabel.text = "00.00";
            isTimerToZero = true;
            return;
        } else
        {
            currentTime -= Time.deltaTime;
            TimeSpan testtime = TimeSpan.FromSeconds(currentTime);
            timerLabel.text = testtime.ToString("ss'.'ff");
        }
    }

    public void StopTimerLabel()
    {
        currentTime = 0f;
        timerLabel.text = "00.00";
        StopCoroutine(UpdateTimer());
        timerLabel.gameObject.SetActive(false);
    }
}
