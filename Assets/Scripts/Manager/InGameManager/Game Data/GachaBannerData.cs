using System.Collections.Generic;

[System.Serializable]
public class GachaBannerDataRoot
{
    public List<GachaBannerData> listGachaBanners;
}

[System.Serializable]
public class GachaBannerData
{
    public int id;
    public string name;
    public List<int> units;
}
