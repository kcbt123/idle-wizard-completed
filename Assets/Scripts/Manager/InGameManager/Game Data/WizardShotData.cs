using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WizardShotDataRoot
{
    public WizardShotData wizardShotData;
}

[System.Serializable]
public class WizardShotData
{
    public int currentLevel=1;
    public string currentPower;
    public List<int> skillTreeBranches;

    /** ======= MARK: - Constructors ======= */

    public WizardShotData()
    {

    }

    public WizardShotData(int currentLevel, string currentPower)
    {
        this.currentLevel = currentLevel;
        this.currentPower = currentPower;
    }
}



