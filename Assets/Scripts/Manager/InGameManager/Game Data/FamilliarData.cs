using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class FamilliarDataRoot
{
    public List<FamilliarData> listFamilliarData;
}

[System.Serializable]
public class FamilliarData
{
    public int index;
    public string id;
    public string name;
    public string rarity;
    public string type;
    public string description;
    public string image;
    public CurrentStats currentStats;
    public List<CurrentPassiveSkillsInfo> currentPassiveSkillsInfo;
    public FamilliarStats stats;
    public List<PassiveSkills> passiveSkills;
}

[System.Serializable]
public class FamilliarStats
{
    public string baseDamage;
    public string CRTRate;
    public string shotSpeed;
    public string shotColor;
    public string shotSize;
    public string shotDelay;
    public SpecialStats specialStats;
}

[System.Serializable]
public class SpecialStats
{
    public string bounceTimes;
}

[System.Serializable]
public class PassiveSkills
{
    public int requiredRank;
    public string name;
    public string effectDesc;
    public SpecialType specialType;
    public string effectAmount;
    public string metrics;
    public string upgradePerLevel;
}

[System.Serializable]
public class SpecialType
{
    public string explosionRadius;
    public string explosionColor;
    public string spawnAmount;
    public string spawnDelay;
    public string spawnRadius;
    public string spreadAmount;
}

[System.Serializable]
public class CurrentStats
{
    public int level;
    public int rank;
    public string shotDamage;
    public string CRTRate;
    public string shotSpeed;
    public string shotDelay;
    public string shotSize;
    public bool unlocked;
}

[System.Serializable]
public class CurrentPassiveSkillsInfo
{
    public int rank;
    public string stats;
}
