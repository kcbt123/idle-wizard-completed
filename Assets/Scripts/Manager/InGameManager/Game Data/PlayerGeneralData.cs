using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GeneralDataRoot
{
    public GeneralData generalData;
}

[System.Serializable]
public class GeneralData
{
    public string playerID;
    public int currentStage=1;
    public string logOutTime;
}


