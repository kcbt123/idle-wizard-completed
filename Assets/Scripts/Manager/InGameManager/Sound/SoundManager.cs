using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }
    private SoundManager()
    {
        if (_instance == null)
            _instance = this;

    }

    /** ======= MARK: - Singleton ======= */

    /// <summary>
    /// Singleton instance
    /// </summary>
    private static SoundManager _instance;

    public static SoundManager instance
    {
        get
        {
            return _instance;
        }
    }

    /** ======= MARK: - Fields and Properties ======= */

    [HideInInspector]
    public AudioSource backgroundMusicSource;
    [HideInInspector]
    public AudioSource buttonSFXSource;
    [HideInInspector]
    public AudioSource wizardShotSFXSource;
    [HideInInspector]
    public AudioSource wizardShotContactSFXSource;

    public SoundList soundList;

    private List<EventListener> _eventListeners;

    /** ======= MARK: - MonoBehaviour Methods ======= */

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        AddListeners();

        backgroundMusicSource = transform.GetChild(0).GetComponents<AudioSource>()[0];
        buttonSFXSource = transform.GetChild(1).GetComponents<AudioSource>()[0];
        wizardShotSFXSource = transform.GetChild(2).GetComponents<AudioSource>()[0];
        wizardShotContactSFXSource = transform.GetChild(2).GetComponents<AudioSource>()[1];

        SetSoundVolume(backgroundMusicSource);
        SetSoundVolume(buttonSFXSource);
        SetSoundVolume(wizardShotSFXSource);
        SetSoundVolume(wizardShotContactSFXSource);
    }

    private void OnDestroy()
    {
        RemoveListeners();
    }

    /** ======= MARK: - Event Listeners ======= */

    private void AddListeners()
    {
        _eventListeners = new List<EventListener>
        {

        };
    }

    private void RemoveListeners()
    {
        if (_eventListeners.Count != 0)
        {
            foreach (EventListener listener in _eventListeners)
                CustomEventSystem.instance.RemoveListener(listener.eventCode, listener);
        }
    }
    private void SetSoundVolume(AudioSource sound)
    {
        string key = "Volume" + sound.name;
        if (PlayerPrefs.HasKey(key))
        {
            sound.volume = PlayerPrefs.GetFloat(key);
        }
    }
}
