using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveDataManager : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private SaveDataManager()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static SaveDataManager _instance;

    public static SaveDataManager instance
    {
        get
        {
            return _instance;
        }
    }
    public bool reset = false;
    /** ======= MARK: - Fields & Properties ======= */

    public static string directory = "/PlayerData/";
    public static string generalFileName = "PlayerGeneralData.json";
    public static string currencyFileName = "PlayerCurrencyData.json";
    public static string inventoryFileName = "PlayerFamilliarData.json";
    public static string equipFileName = "EquipFamilliarData.json";
    public static string wizardShotFileName = "WizardShotData.json";

    /** ======= MARK: - Save Data Functions ======= */
    public void SaveFamilliarData_Overwrite(List<FamilliarData> data)
    {
        string dataString = JSONUtil.ToJsonArray(data, true);
        DoSaveData(inventoryFileName, dataString);
    }

    public void AddFamilliarFromGacha(List<FamilliarData> data)
    {
        List<FamilliarData> currentInventory = LoadFamilliarInventoryData();
        currentInventory.AddRange(data);
        string dataString = JSONUtil.ToJsonArray(currentInventory, true);
        DoSaveData(inventoryFileName, dataString);
    }

    public void SaveEquippedFamilliar(List<int> data)
    {
        string dataString = JSONUtil.ToJsonArray(data, true);
        DoSaveData(equipFileName, dataString);
    }

    public void SaveGeneralData(GeneralData data)
    {
        string dataString = JsonUtility.ToJson(data, true);
        DoSaveData(generalFileName, dataString);
    }

    public void SaveCurrencyData(CurrencyData data)
    {
        string dataString = JsonUtility.ToJson(data, true);
        DoSaveData(currencyFileName, dataString);
    }

    public void SaveWizardShotData(WizardShotData data)
    {
        string dataString = JsonUtility.ToJson(data, true);
        DoSaveData(wizardShotFileName, dataString);
    }

    private void DoSaveData(string filename, string dataString)
    {
        string dir = Application.persistentDataPath + directory;
        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        JSONUtil.WrileFile(dir + filename, dataString);
        Debug.Log("Succesfully saved data to " + dir + filename);
    }

    /** ======= MARK: - Load Data Functions ======= */

    public GeneralData LoadPlayerGeneralData()
    {
        GeneralData data = new GeneralData();
        string jsonData = DoLoadData(generalFileName);
        if (jsonData != "")
        {
            data = JsonUtility.FromJson<GeneralData>(jsonData);
        }
        return data;
    }

    public List<FamilliarData> LoadFamilliarInventoryData()
    {
        List<FamilliarData> data = new List<FamilliarData>();
        string jsonData = DoLoadData(inventoryFileName);
        if (jsonData != "")
        {
            Wrapper<FamilliarData> wrapper = JsonUtility.FromJson<Wrapper<FamilliarData>>(jsonData);
            FamilliarData[] familliarData = wrapper.Items;

            for (int i = 0; i < familliarData.Length; i++)
            {
                data.Add(familliarData[i]);
            }
        } else
        {
            Debug.Log(inventoryFileName + "does not exists!!!");
        }

        return data;
    }

    public List<int> LoadEquippedFamilliarData()
    {
        List<int> data = new List<int>();
        string jsonData = DoLoadData(equipFileName);
        if (jsonData != "")
        {
            Wrapper<int> wrapper = JsonUtility.FromJson<Wrapper<int>>(jsonData);
            int[] familliarData = wrapper.Items;
            for (int i = 0; i < familliarData.Length; i++)
            {
                data.Add(familliarData[i]);
            }
        } else
        {
            Debug.Log(equipFileName + "does not exists!!!");
        }

        return data;
    }

    public WizardShotData LoadWizardShotData()
    {
        WizardShotData data = new WizardShotData();
        string jsonData = DoLoadData(wizardShotFileName);
        if (jsonData != "")
        {
            data = JsonUtility.FromJson<WizardShotData>(jsonData);
        }

        return data;
    }

    public string DoLoadData(string fileName)
    {
        string fullpath = Application.persistentDataPath + directory + fileName;
        if (File.Exists(fullpath))
        {
            return File.ReadAllText(fullpath);
        }

        return "";
    }
    public void DeleteAllFile()
    {
        File.Delete(Application.persistentDataPath + directory + generalFileName);
        File.Delete(Application.persistentDataPath + directory + currencyFileName);
        File.Delete(Application.persistentDataPath + directory + inventoryFileName);
        File.Delete(Application.persistentDataPath + directory + equipFileName);
        File.Delete(Application.persistentDataPath + directory + wizardShotFileName);
        reset = true;
    }

private class Wrapper<T>
    {
        public T[] Items;
    }
}
