using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageDataManager : MonoBehaviour
{
    /** ======= MARK: - Init ======= */
    public string GetClassName()
    {
        return this.GetType().Name;
    }

    private StageDataManager()
    {
        if (_instance == null)
            _instance = this;
    }

    /** ======= MARK: - Singleton ======= */

    private static StageDataManager _instance;

    public static StageDataManager instance
    {
        get
        {
            return _instance;
        }
    }

    /** ======= MARK: - Fields and properties ======= */

    private const string BASE_MONSTER_HP = "40";
    private const string BASE_BOSS_HP = "100";

    private const string BASE_MONSTER_GOLD_REWARD = "2";
    private const string BASE_BOSS_GOLD_REWARD = "6";
    private const string BASE_BOSS_DIAMOND_REWARD = "50";

    public int currentStage = 1;
    public string currentMonsterHP = BASE_MONSTER_HP;
    public string currentStageReward = BASE_MONSTER_GOLD_REWARD;

    /** ======= MARK: - MonoBehavior Methods ======= */

    private void Start()
    {
        InitStageData();
    }

    /** ======= MARK: - Handle Stage Data ======= */

    private void InitStageData()
    {
    //HieuBT: -Load Data Normal
        GeneralData savedStageData = SaveDataManager.instance.LoadPlayerGeneralData();
        if (savedStageData != null)
        {
            currentStage = savedStageData.currentStage;
            currentMonsterHP = CalculateMonsterHP(savedStageData.currentStage);
            currentStageReward = CalculateGoldReward(savedStageData.currentStage);
            GameObject.Find(GameObjectList.BOSS_HP_BAR).transform.GetChild(2).GetComponent<TMPro.TextMeshProUGUI>().text = currentMonsterHP;
            GameObject.Find(GameObjectList.LABEL_CURRENT_STAGE).GetComponent<TMPro.TextMeshProUGUI>().text = "Current Stage: " + currentStage;
        }
        else
        {
            currentStage = 1;
            currentMonsterHP = BASE_MONSTER_HP;
            currentStageReward = BASE_MONSTER_GOLD_REWARD;
        }
    }

    /** ======= MARK: - Stage Data Calculation ======= */
    public string CalculateMonsterHP(int stage)
    {
        string monsterHP = "";

        // HieuBT: - Day la phan de nhan theo stat goc
        int baseToPower = stage / 5;
        float power = Mathf.Pow(2.0f, baseToPower);

        // HieuBT: - Level 1-4x thi HP giong nhau, Level 5x thi theo base cua boss
        if (stage % 5 != 0)
        {
            monsterHP = LargeNumberStaticUtil.Multiplication(BASE_MONSTER_HP, power);
        } else
        {
            monsterHP = LargeNumberStaticUtil.Multiplication(BASE_BOSS_HP, power - 1);
        }

        return monsterHP;
    }

    public string CalculateGoldReward(int stage)
    {
        string reward = "";

        // HieuBT: - Day la phan de nhan theo stat goc
        int baseToPower = stage / 5;
        float power = Mathf.Pow(2.0f, baseToPower);

        // HieuBT: - Level 1-4x thi reward giong nhau, Level 5x thi theo base cua boss
        if (stage % 5 != 0)
        {
            reward = LargeNumberStaticUtil.Multiplication(BASE_MONSTER_GOLD_REWARD, power);
        }
        else
        {
            reward = LargeNumberStaticUtil.Multiplication(BASE_BOSS_GOLD_REWARD, power - 1);
        }

        return reward;
    }

    public string CalculateBossDiamondReward(int stage)
    {
        string reward = "";

        // HieuBT: - Level 1-4x thi ko reward diamond, moi lan giet boss thi cho 50 diamond
        if (stage % 5 != 0)
        {
            reward = "0";
        }
        else
        {
            reward = BASE_BOSS_DIAMOND_REWARD;
        }

        return reward;
    }
}
