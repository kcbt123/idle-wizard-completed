using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class FamilliarShotUpgradeController : MonoBehaviour
{
    public int currentFamilliarShotLevel;
    public string currentFamilliarShotPower;
    [SerializeField]
    private GameObject popupNotEnough;
    [SerializeField]
    private GameObject familliarUpgradePanel;

    private static string BASE_FAMILLIAR_SHOT_UPGRADE_COST = "2.00";
    private static float COST_MODIFIER = 1.5f;

    private static string BASE_FAMILLIAR_SHOT_POWER;
    private static float POWER_MODIFIER = 2.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void UpgradeFamilliarShot(int pos)
    {
        DoUpgradeFamilliarShotByLevel(pos, 1);
    }
    public void UpgradeFamilliarShot10(int pos)
    {
        DoUpgradeFamilliarShotByLevel(pos, 10);
    }
    public void DoUpgradeFamilliarShotByLevel(int pos, int level)
    {
        if (FamilliarEquipController.instance.equipedList[pos] < 0) return;
        FamilliarData currentFamilliar = FamilliarEquipController.instance.equipFamilliar[FamilliarEquipController.instance.equipedList[pos]];
        currentFamilliarShotLevel = currentFamilliar.currentStats.level;
        
        string upgradeCost; //Tinh tien de nang cap
        if (level == 1)
        {
            upgradeCost = CalculateUpgradeCost(currentFamilliarShotLevel + level); // Level sau khi da cong

        }
        else upgradeCost = CalculateTenMoreConsecutiveCost(currentFamilliarShotLevel + 1);

        if (LargeNumberStaticUtil.CompareNumber(CurrencyController.currency.gold, upgradeCost) == LargeNumberResult.FALSE)
        {
            popupNotEnough.SetActive(true);
            popupNotEnough.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Not enough gold!";
            return;
        }
        BASE_FAMILLIAR_SHOT_POWER = currentFamilliar.stats.baseDamage;
        currentFamilliarShotLevel += level;
        currentFamilliarShotPower = CalculateFamilliarShotPower(currentFamilliarShotLevel); // Level sau khi da cong
        currentFamilliar.currentStats.level = currentFamilliarShotLevel;
        currentFamilliar.currentStats.shotDamage = currentFamilliarShotPower;
        FamilliarEquipController.instance.equipFamilliar[FamilliarEquipController.instance.equipedList[pos]] = currentFamilliar;

        SaveDataManager.instance.SaveFamilliarData_Overwrite(FamilliarEquipController.instance.equipFamilliar);

        Transform upgradeFamilliarDes = familliarUpgradePanel.transform.GetChild(pos).GetChild(0).GetChild(1);
        Transform upgrade1CostLabel = familliarUpgradePanel.transform.GetChild(pos).GetChild(0).GetChild(2);
        Transform upgrade10CostLabel = familliarUpgradePanel.transform.GetChild(pos).GetChild(0).GetChild(3);
        upgradeFamilliarDes.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Level: " + currentFamilliarShotLevel;
        upgrade1CostLabel.GetChild(1).GetComponent<TextMeshProUGUI>().text = CalculateUpgradeCost(currentFamilliarShotLevel + 1);
        upgrade10CostLabel.GetChild(1).GetComponent<TextMeshProUGUI>().text = CalculateTenMoreConsecutiveCost(currentFamilliarShotLevel + 1);

        CurrencyController.RemoveGold(upgradeCost);
        CustomEventSystem.instance.DispatchEvent(EventCode.ON_UPGRADE_FAMILLIAR, new object[] {
                currentFamilliarShotPower,
                pos
            });
    }
    private string CalculateFamilliarShotPower(int familliarShotLevel)
    {
        string result = "0";
        float powerRaised = familliarShotLevel / 10;

        result = LargeNumberStaticUtil.Multiplication(BASE_FAMILLIAR_SHOT_POWER, familliarShotLevel);
        while (powerRaised > 20)
        {
            result = LargeNumberStaticUtil.Multiplication(result, Mathf.Pow(POWER_MODIFIER, 20));
            powerRaised -= 20;
        }
        float powerCurrent = Mathf.Pow(POWER_MODIFIER, powerRaised);
        result = LargeNumberStaticUtil.Multiplication(result, powerCurrent);
        
        return result;
    }

    private string CalculateUpgradeCost(int familliarShotLevel) // Tinh toan cost de upgrade len level la wizardShotLevel
    {
        string result = "0";
        string additive = LargeNumberStaticUtil.Subtraction(BASE_FAMILLIAR_SHOT_UPGRADE_COST, "1.00");
        float powerRaised = familliarShotLevel / 10;

        string convertedLevel = familliarShotLevel.ToString();
        result = LargeNumberStaticUtil.Subtraction(convertedLevel, "1.00");
        result = LargeNumberStaticUtil.Addition(result, additive);
        while (powerRaised > 20)
        {
            result = LargeNumberStaticUtil.Multiplication(result, Mathf.Pow(COST_MODIFIER, 20));
            powerRaised -= 20;
        }
        float powerCurrent = Mathf.Pow(COST_MODIFIER, powerRaised);
        result = LargeNumberStaticUtil.Multiplication(result, powerCurrent);

        return result;
    }

    private string CalculateTenMoreConsecutiveCost(int familliarShotLevel) // Tinh toan cost de upgrade len 10 level, tinh tu level be hon input
                                                                           // Vi du: CalculateTenMoreConsecutiveCost(2) la tinh tong upgrade cost cho den level 12
    {
        string result = "0";

        for (int i = 0; i < 10; i++)
        {
            result = LargeNumberStaticUtil.Addition(result, CalculateUpgradeCost(familliarShotLevel + i));
        }

        return result;
    }
    public void SetupUpgradeFamilliar()
    {
        Debug.Log("============Setup" );
        for (int i = 0; i < 4; i++)
        {
            Transform upgrade1CostLabel = familliarUpgradePanel.transform.GetChild(i).GetChild(0).GetChild(2);
            Transform upgrade10CostLabel = familliarUpgradePanel.transform.GetChild(i).GetChild(0).GetChild(3);
            if (FamilliarEquipController.instance.equipedList[i] < 0)
            {
                upgrade1CostLabel.GetChild(1).GetComponent<TextMeshProUGUI>().text = "0";
                upgrade10CostLabel.GetChild(1).GetComponent<TextMeshProUGUI>().text = "0";
                continue;
            }
            Debug.Log("============" + FamilliarEquipController.instance.equipedList[i]);
            FamilliarData currentFamilliar = FamilliarEquipController.instance.equipFamilliar[FamilliarEquipController.instance.equipedList[i]];
            upgrade1CostLabel.GetChild(1).GetComponent<TextMeshProUGUI>().text = CalculateUpgradeCost(currentFamilliar.currentStats.level + 1);
            upgrade10CostLabel.GetChild(1).GetComponent<TextMeshProUGUI>().text = CalculateTenMoreConsecutiveCost(currentFamilliar.currentStats.level + 1);
        }
    }
}
