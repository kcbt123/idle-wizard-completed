using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WizardShotUpgradeController : MonoBehaviour
{
    /** ======= MARK: - Fields and Properties ======= */

    private static int BASE_WIZARD_SHOT_LEVEL = 1;

    private static string BASE_WIZARD_SHOT_POWER = "1.00";
    private static float POWER_MODIFIER = 2.0f;

    private static string BASE_WIZARD_SHOT_UPGRADE_COST = "5.00"; // Cost de upgrade tu Lvl 1 len Lvl 2 
    private static float COST_MODIFIER = 3.0f;

    public int currentWizardShotLevel;
    public string currentWizardShotPower;
    public WizardShotData wizardShotData;
    [SerializeField]
    private GameObject popupNotEnough;

    [SerializeField]
    private TextMeshProUGUI dpsLabel;

    [SerializeField]
    private TextMeshProUGUI currentLevelLabel;

    [SerializeField]
    private TextMeshProUGUI upgrade1CostLabel;

    [SerializeField]
    private TextMeshProUGUI upgrade10CostLabel;

    /** ======= MARK: - MonoBehaviour Methods ======= */

    // Start is called before the first frame update
    void Start()
    {
        wizardShotData = SaveDataManager.instance.LoadWizardShotData();
        if (wizardShotData.currentLevel != 1)
        {
            currentWizardShotLevel = wizardShotData.currentLevel;
            currentWizardShotPower = wizardShotData.currentPower;
        }
        else
        {
            currentWizardShotLevel = BASE_WIZARD_SHOT_LEVEL;
            currentWizardShotPower = BASE_WIZARD_SHOT_POWER;
        }

        currentLevelLabel.text = "Current Level: " + currentWizardShotLevel;
        dpsLabel.text = "Current DPS: " + currentWizardShotPower;
        upgrade1CostLabel.text = CalculateUpgradeCost(currentWizardShotLevel + 1);
        upgrade10CostLabel.text = CalculateTenMoreConsecutiveCost(currentWizardShotLevel + 1);
    }

    /** ======= MARK: - Upgrade Methods ======= */

    public void UpgradeWizardShot()
    {
        DoUpgradeWizardShotByLevel(1);
    }
    public void UpgradeWizardShot10()
    {
        DoUpgradeWizardShotByLevel(10);
    }

    public void DoUpgradeWizardShotByLevel(int level)
    {
        string upgradeCost;
        if (level == 1)
        {
            upgradeCost = CalculateUpgradeCost(currentWizardShotLevel + level); // Level sau khi da cong

        }
        else upgradeCost = CalculateTenMoreConsecutiveCost(currentWizardShotLevel+1);
        if (LargeNumberStaticUtil.CompareNumber(CurrencyController.currency.gold, upgradeCost)==LargeNumberResult.FALSE)
        {
            popupNotEnough.SetActive(true);
            popupNotEnough.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "Not enough gold!";
            return;
        }
        currentWizardShotLevel += level;
        currentWizardShotPower = CalculateWizardShotPower(currentWizardShotLevel); // Level sau khi da cong

        WizardShotData data = new WizardShotData(currentWizardShotLevel, currentWizardShotPower);
        SaveDataManager.instance.SaveWizardShotData(data);

        currentLevelLabel.text = "Current Level: " + currentWizardShotLevel;
        dpsLabel.text = "Current DPS: " + currentWizardShotPower;
        upgrade1CostLabel.text = CalculateUpgradeCost(currentWizardShotLevel + 1);
        upgrade10CostLabel.text = CalculateTenMoreConsecutiveCost(currentWizardShotLevel + 1);

        CurrencyController.RemoveGold(upgradeCost);
    }

    /** ======= MARK: - Calculation Methods ======= */

    private string CalculateWizardShotPower(int wizardShotLevel)
    {
        string result = "0";
        float powerRaised = wizardShotLevel / 10;

        result = wizardShotLevel.ToString();
        while (powerRaised > 20)
        {
            result = LargeNumberStaticUtil.Multiplication(result, Mathf.Pow(POWER_MODIFIER, 20));
            powerRaised -= 20;
        }
        float powerCurrent = Mathf.Pow(POWER_MODIFIER, powerRaised);
        result = LargeNumberStaticUtil.Multiplication(result, powerCurrent);

        return result;
    }

    private string CalculateUpgradeCost(int wizardShotLevel) // Tinh toan cost de upgrade len level la wizardShotLevel
    {
        string result = "0";
        string additive = LargeNumberStaticUtil.Subtraction(BASE_WIZARD_SHOT_UPGRADE_COST, "1.00");
        float powerRaised = wizardShotLevel / 10;

        string convertedLevel = wizardShotLevel.ToString();
        result = LargeNumberStaticUtil.Subtraction(convertedLevel, "1.00");
        result = LargeNumberStaticUtil.Addition(result, additive);
        while (powerRaised > 20)
        {
            result = LargeNumberStaticUtil.Multiplication(result, Mathf.Pow(COST_MODIFIER, 20));
            powerRaised -= 20;
        }
        float powerCurrent = Mathf.Pow(COST_MODIFIER, powerRaised);
        result = LargeNumberStaticUtil.Multiplication(result, powerCurrent);

        return result;
    }

    private string CalculateTenMoreConsecutiveCost(int wizardShotLevel) // Tinh toan cost de upgrade len 10 level, tinh tu level be hon input
                                                                           // Vi du: CalculateTenMoreConsecutiveCost(2) la tinh tong upgrade cost cho den level 12
    {
        string result = "0";

        for (int i = 0; i < 10; i++)
        {
            result = LargeNumberStaticUtil.Addition(result, CalculateUpgradeCost(wizardShotLevel + i));
        }

        return result;
    }
    void OnApplicationQuit()
    {
        if (SaveDataManager.instance.reset) return;
        wizardShotData.currentLevel = currentWizardShotLevel;
        wizardShotData.currentPower = currentWizardShotPower;
        SaveDataManager.instance.SaveWizardShotData(wizardShotData);
    }
}
