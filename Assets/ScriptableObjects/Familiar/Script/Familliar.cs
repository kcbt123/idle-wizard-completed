using UnityEngine;

[CreateAssetMenu(fileName = "Familliar", menuName = "idle-wizard/Familliar", order = 0)]
public class Familliar : ScriptableObject
{
    public string Name;
    public Sprite FamilliarImage;
    public int Level;
    public int RankedUpLevel;
    public string Rarity;
    private int id;
    public string PrintFamilliarInfo()
    {
        string info = "Familliar Name: " + Name + "/n";
        info += "Familliar Level: " + Level + "/n";
        info += "Familliar Ranked Up Level: " + RankedUpLevel + "/n";
        info += "Familliar Rarity: " + Rarity + "/n";
        return info;
    }
    public void SetFamilliarId(int NewId)
    {
        id = NewId;
    }
}